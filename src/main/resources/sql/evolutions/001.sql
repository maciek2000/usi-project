CREATE SEQUENCE hibernate_sequence;

CREATE TABLE url_redirect
(
    id         BIGINT PRIMARY KEY       NOT NULL,
    created    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    identifier VARCHAR UNIQUE           NOT NULL,
    full_url   VARCHAR                  NOT NULL
);

