DELETE FROM url_redirect WHERE created IS NOT NULL;

ALTER TABLE url_redirect
    ADD COLUMN short_url VARCHAR;

ALTER TABLE url_redirect
    ADD COLUMN visit_count BIGINT DEFAULT 0;

CREATE TABLE users
(
    id       BIGINT PRIMARY KEY,
    uuid     UUID UNIQUE              NOT NULL,
    email    VARCHAR UNIQUE           NOT NULL,
    password VARCHAR                  NOT NULL,
    created  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

ALTER TABLE url_redirect
    ADD COLUMN user_id BIGINT
        CONSTRAINT fk_url_redirect_user_id REFERENCES users (id);


