package pl.project.usi.url_shortener;


import pl.project.usi.url_shortener.dto.URLRedirectDto;

import java.util.List;
import java.util.UUID;

public interface URLRedirectFacade {

    String shortenURLAddress(String url, String userUUID);

    String getURLByIdentifier(String identifier);

    List<URLRedirectDto> getUserUrls(UUID userUUID);

    Long getTotalVisitCount();
}
