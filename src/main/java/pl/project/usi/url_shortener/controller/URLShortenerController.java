package pl.project.usi.url_shortener.controller;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.project.usi.url_shortener.URLRedirectFacade;
import pl.project.usi.url_shortener.dto.URLRedirectDto;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class URLShortenerController {

    private URLRedirectFacade urlRedirectFacade;

    @PostMapping("/shorten")
    public String shortenUrl(@RequestParam(name = "url") String url, @RequestParam(name = "userUUID", required = false) String userUUID) {
        return urlRedirectFacade.shortenURLAddress(url, userUUID);
    }

    @GetMapping("/shorten")
    public String getFullUrl(@RequestParam(name = "identifier") String identifier) {
        return urlRedirectFacade.getURLByIdentifier(identifier);
    }

    @GetMapping("/url/users/{userUUID}")
    public List<URLRedirectDto> getUserUrls(@PathVariable(name = "userUUID") UUID userUUID) {
        return urlRedirectFacade.getUserUrls(userUUID);
    }

    @GetMapping("/shorten/total")
    public Long getTotalVisitCount() {
        return urlRedirectFacade.getTotalVisitCount();
    }
}
