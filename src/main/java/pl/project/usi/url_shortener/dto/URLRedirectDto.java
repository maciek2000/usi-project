package pl.project.usi.url_shortener.dto;

public record URLRedirectDto(
        String identifier,
        String fullURL,
        String shortURL,
        Long visitCount
){}
