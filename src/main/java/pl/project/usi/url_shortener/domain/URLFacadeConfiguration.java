package pl.project.usi.url_shortener.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.project.usi.url_shortener.URLRedirectFacade;
import pl.project.usi.user.domain.UserRepository;

@Configuration
public class URLFacadeConfiguration {

    @Bean
    public URLRedirectFacade urlRedirectFacade(URLRedirectRepository urlRedirectRepository, UserRepository userRepository) {
        return new URLRedirectService(urlRedirectRepository, userRepository);
    }
}
