package pl.project.usi.url_shortener.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import pl.project.usi.user.domain.UserEntity;

import java.time.Clock;
import java.time.OffsetDateTime;

@Getter
@Setter
@Entity
@Table(name = "url_redirect")
public class URLRedirectEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hibernate_sequence")
    @SequenceGenerator(name = "hibernate_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "created", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime created = OffsetDateTime.now(Clock.systemUTC());

    @Column(name = "identifier", nullable = false, unique = true)
    private String identifier;

    @Column(name = "full_url", nullable = false)
    private String fullURL;

    @Column(name = "short_url", nullable = false)
    private String shortURL;

    @Column(name = "visit_count", nullable = false)
    private Long visitCount = 0L;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;
}