package pl.project.usi.url_shortener.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface URLRedirectRepository extends JpaRepository<URLRedirectEntity, Long> {

    boolean existsByIdentifier(String identifier);

    Optional<URLRedirectEntity> findByIdentifier(String identifier);

    @Query(
            """
                            SELECT entity FROM URLRedirectEntity entity
                            LEFT JOIN FETCH entity.user users
                            WHERE users.uuid = :userUUID
                    """
    )
    List<URLRedirectEntity> findAllByUserUUID(UUID userUUID);
}
