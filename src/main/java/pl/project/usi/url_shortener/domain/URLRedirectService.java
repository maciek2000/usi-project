package pl.project.usi.url_shortener.domain;

import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.project.usi.common.exception.NotFoundException;
import pl.project.usi.common.validator.URLValidator;
import pl.project.usi.url_shortener.URLRedirectFacade;
import pl.project.usi.url_shortener.dto.URLRedirectDto;
import pl.project.usi.user.domain.UserEntity;
import pl.project.usi.user.domain.UserRepository;

import java.security.SecureRandom;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;

@AllArgsConstructor
class URLRedirectService implements URLRedirectFacade {

    private final URLRedirectRepository urlRedirectRepository;
    private final UserRepository userRepository;

    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int LENGTH = 6;
    private static final SecureRandom random = new SecureRandom();

    @Override
    public String shortenURLAddress(String url, String userUUID) {
        URLValidator.validateURL(url);

        URLRedirectEntity entity = new URLRedirectEntity();
        String identifier = generateIdentifier();
        entity.setUser(getUser(userUUID));
        entity.setFullURL(url);
        entity.setIdentifier(identifier);
        String shortURL = getShortURL(identifier);
        entity.setShortURL(shortURL);

        urlRedirectRepository.save(entity);
        return shortURL;
    }

    @Override
    @Transactional
    public String getURLByIdentifier(String identifier) {
        URLRedirectEntity entity = urlRedirectRepository.findByIdentifier(identifier)
                .orElseThrow(() -> new NotFoundException(String.format("URL with identifier %s not found", identifier)));
        entity.setVisitCount(entity.getVisitCount() + 1);
        return entity.getFullURL();
    }

    @Override
    public List<URLRedirectDto> getUserUrls(UUID userUUID) {
        return urlRedirectRepository.findAllByUserUUID(userUUID).stream()
                .map(url -> new URLRedirectDto(url.getIdentifier(), url.getFullURL(), url.getShortURL(), url.getVisitCount()))
                .toList();
    }

    @Override
    public Long getTotalVisitCount() {
        return urlRedirectRepository.findAll().stream()
                .map(URLRedirectEntity::getVisitCount)
                .reduce(0L, Long::sum);
    }

    private UserEntity getUser(String userUUID) {
        if (nonNull(userUUID)) {
            return userRepository.findByUuid(UUID.fromString(userUUID))
                    .orElseThrow(() -> new NotFoundException(String.format("User with uuid %s not found", userUUID)));
        } else {
            return null;
        }
    }

    private String getShortURL(String identifier) {
        return "http://127.0.0.1:5500/redirect.html#" + identifier;
    }

    public String generateIdentifier() {
        String identifier = random.ints(LENGTH, 0, CHARACTERS.length())
                .mapToObj(CHARACTERS::charAt)
                .map(Object::toString)
                .collect(joining());

        if (urlRedirectRepository.existsByIdentifier(identifier)) {
            return generateIdentifier();
        } else {
            return identifier;
        }
    }
}
