package pl.project.usi.user.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.project.usi.user.UserFacade;
import pl.project.usi.user.dto.CreateUserForm;
import pl.project.usi.user.dto.LoginForm;

import java.util.UUID;

@RestController
@AllArgsConstructor
public class UserController {

    private UserFacade userFacade;

    @PostMapping("/user")
    public void createUser(@RequestBody CreateUserForm form) {
        userFacade.createUser(form);
    }

    @GetMapping("/login")
    public UUID login(@RequestBody LoginForm form) {
        return userFacade.login(form);
    }
}
