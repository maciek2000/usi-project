package pl.project.usi.user;

import pl.project.usi.user.dto.CreateUserForm;
import pl.project.usi.user.dto.LoginForm;

import java.util.UUID;

public interface UserFacade {

    void createUser(CreateUserForm form);

    UUID login(LoginForm form);
}
