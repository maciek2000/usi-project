package pl.project.usi.user.dto;

public record LoginForm(String email, String password) {
}
