package pl.project.usi.user.dto;

public record CreateUserForm(String email, String password) {

}
