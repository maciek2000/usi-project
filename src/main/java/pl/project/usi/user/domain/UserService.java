package pl.project.usi.user.domain;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.project.usi.common.exception.InvalidLoginDataException;
import pl.project.usi.common.exception.NotFoundException;
import pl.project.usi.common.exception.NotUniqueException;
import pl.project.usi.user.UserFacade;
import pl.project.usi.user.dto.CreateUserForm;
import pl.project.usi.user.dto.LoginForm;

import java.util.UUID;

@AllArgsConstructor
public class UserService implements UserFacade {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void createUser(CreateUserForm form) {
        validateEmail(form);

        UserEntity user = new UserEntity();
        user.setEmail(form.email());
        user.setPassword(passwordEncoder.encode(form.password()));
        userRepository.save(user);
    }

    @Override
    public UUID login(LoginForm form) {
        UserEntity user = userRepository.findByEmail(form.email())
                .orElseThrow(() -> new NotFoundException(String.format("User with email: %s not found", form.email())));
        if (passwordEncoder.matches(form.password(), user.getPassword())) {
            return user.getUuid();
        } else {
            throw new InvalidLoginDataException("Provided login data is invalid");
        }
    }

    private void validateEmail(CreateUserForm form) {
        if (userRepository.existsByEmail(form.email())) {
            throw new NotUniqueException(String.format("Provided email address already exists in database: %s", form.email()));
        }
    }
}
