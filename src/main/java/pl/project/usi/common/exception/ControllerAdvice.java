package pl.project.usi.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(InvalidURLException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String invalidUrlException(InvalidURLException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String notFoundException(NotFoundException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(NotUniqueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String notUniqueException(NotUniqueException exception) {
        return exception.getMessage();
    }

    @ExceptionHandler(InvalidLoginDataException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected String invalidLoginDataException(InvalidLoginDataException exception) {
        return exception.getMessage();
    }
}
