package pl.project.usi.common.exception;

public class NotUniqueException extends RuntimeException {

    public NotUniqueException(String message) {
        super(message);
    }
}
