package pl.project.usi.common.exception;

public class InvalidLoginDataException extends RuntimeException {
    public InvalidLoginDataException(String message) {
        super(message);
    }
}
