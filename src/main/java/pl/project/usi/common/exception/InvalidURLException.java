package pl.project.usi.common.exception;

public class InvalidURLException extends RuntimeException {

    public InvalidURLException(String message) {
        super(message);
    }
}
