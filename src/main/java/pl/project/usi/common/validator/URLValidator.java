package pl.project.usi.common.validator;

import pl.project.usi.common.exception.InvalidURLException;

import java.util.regex.Pattern;

import static java.util.Objects.isNull;

public class URLValidator {

    private static final String URL_REGEX =
            "^((http|https)://)?" +
                    "(([\\w\\d]([\\w\\d-]*[\\w\\d])*)\\.)+" +
                    "[a-zA-Z]{2,}" +
                    "(:\\d+)?(/.*)?$";

    private static final Pattern URL_PATTERN = Pattern.compile(URL_REGEX);

    public static void validateURL(String url) {
        if (isNull(url) || url.isEmpty() || !URL_PATTERN.matcher(url).matches()) {
            throw new InvalidURLException("Provided URL address is invalid");
        }
    }
}
